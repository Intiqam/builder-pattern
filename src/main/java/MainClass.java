public class MainClass {
    public static void main(String[] args) {
        User user =User.builder()
                .id(1)
                .name("John")
                .surname("Snow")
                .birthDate("1990-28-12")
                .build();
        System.out.println(user);
    }

}
