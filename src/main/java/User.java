import lombok.Data;

@Data
public class User {
    private Long id;
    private String name;
    private String surname;
    private String birthDate;
    private String birtPlace;
    private String nationality;

    public User(UserBuilder userBuilder) {
        this.id = userBuilder.id;
        this.name = userBuilder.name;
        this.surname = userBuilder.surname;
        this.birthDate = userBuilder.birthDate;
        this.birtPlace = userBuilder.birthPlace;
        this.nationality = userBuilder.nationality;

    }

    public static UserBuilder builder() {
        return new UserBuilder();
    }

    public static class UserBuilder{
        private Long id;
        private String name;
        private String surname;
        private String birthDate;
        private String birthPlace;
        private String nationality;

        public UserBuilder id(long id){
            this.id = id;
            return this;
        }
        public UserBuilder name(String name){
            this.name = name;
            return this;
        }
        public UserBuilder surname(String surname){
            this.surname = surname;
            return this;
        }
        public UserBuilder birthDate(String birthDate){
            this.birthDate = birthDate;
            return this;
        }
        public UserBuilder birthPlace(String birtPlace){
            this.birthPlace = birtPlace;
            return this;
        }
        public UserBuilder nationality(String nationality){
            this.nationality = nationality;
            return this;
        }
        public User build(){
            return new User(this);
        }
    }

}
